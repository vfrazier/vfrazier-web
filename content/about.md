+++
title = "About"
description = "Hugo, the world's fastest framework for building websites"
date = "2023-03-01"
aliases = ["about-us", "about-hugo", "contact"]
author = "Frazier Smith"
+++

Frazier Smith is a Senior Manager of Technical Content Development at VMware, where he oversees the creation and maintenance of all VMware customer-facing courses covering vSphere, VMware Cloud, and HCX portfolios. This courseware is also used in the popular VMware IT Academy throughout the world.

He manages a team of 12 instructional designers and technical content developers for the creation of online and instructor-led training materials, and has implemented industry-standard learning tooling to enable teams to curate and operationalize existing learning content into digital modalities. He has successfully revamped organizational structures and developed departmental SOPs for consistent content creation and processes for demand scheduling and resource utilization metrics.

Additionally, Frazier has extensive experience in the world of Information Technology. Frazier earned his Cisco Certified Network Associate at the age of 16 and studied Network Administration in high school at Highland School of Technology. Frazier has achieved VMware certifications in DCV, NV, and DW, and has delivered highly technical courses globally with a consistently high instructor rating of 4.95+/5.

His current areas of interest are Kubernetes, Multi-Cloud, and hybrid datacenter design.

Frazier holds a Master of Education degree in Instructional Systems Technology with a focus on Training and Development from the University of North Carolina at Charlotte, a Master of Science in Information Technology Management from Western Governors University and a Bachelor of Music Education degree in Choral Music from Wingate University. He lives in Mount Holly, North Carolina, with his wife, Olivia, and his two cats, Roscoe and Gracie.