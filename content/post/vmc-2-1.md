---
title: "VCP-VMC 2.1: Cloud Operating Model" # Title of the blog post.
date: 2023-03-07T14:22:48-05:00 # Date of post creation.
description: "This article explores the Cloud Operating Model, which is a crucial aspect of a successful IT transformation initiative. It includes the people, processes, and technology required to create, manage, and deliver cloud services, leveraging the capabilities of the Cloud Platform." # Description used for search engine.
featured: true # Sets if post is a featured post, making appear on the home page side bar.
draft: false # Sets whether to render this page. Draft of true will not be rendered.
toc: true # Controls if a table of contents should be generated for first-level links automatically.
# menu: main
usePageBundles: false # Set to true to group assets like images in the same folder as this post.
featureImage: "/images/featured/vmc/012.png" # Sets featured image on blog post.
featureImageAlt: 'Description of image' # Alternative text for featured image.
featureImageCap: 'This is the featured image.' # Caption (optional).
thumbnail: "/images/featured/vmc/012.png" # Sets thumbnail image appearing inside card on homepage.
shareImage: "/images/featured/vmc/012.png" # Designate a separate image for social media sharing.
codeMaxLines: 10 # Override global value for how many lines within a code block before auto-collapsing.
codeLineNumbers: false # Override global value for showing of line numbers within code block.
figurePositionShow: true # Override global value for showing the figure label.
categories:
  - VCP-VMC
tags:

# comment: false # Disable comment if false.
---

The Cloud Operating Model is a crucial aspect of a successful IT transformation initiative. It is the blueprint for organizing effectively and delivering on the Cloud Strategy. The Cloud Strategy aligns business outcomes with technical outcomes, whereas the Cloud Operating Model defines the operational processes required to execute on the strategy. To maximize the benefits from the adoption or creation of cloud services, the Cloud Operating Model needs to consist of people, processes, and technology.

The Cloud Platform is the technology platform that supports the delivery and operations of IT services that adhere to the principles of cloud computing. It is a critical component of the Cloud Operating Model, supporting the creation and management of cloud services with the technical capabilities required for efficient operations. The VMware Cloud Management Portfolio provides all the capabilities necessary for a comprehensive cloud platform.

## People
People are still the most critical component of the Cloud Operating Model, despite the emphasis on automation. The organizational structure must change to operate effectively and prioritize communication and collaboration. New roles may need to be defined and skillsets recruited or trained. Clear lines of accountability and responsibility must be established, and stakeholders and consumers should be well understood. Consumer personas, who will be using the services, must be well-defined, including their needs, usage, and consumption preferences. This understanding is necessary to meet their needs and to provision resources efficiently and cost-effectively.

## Processes
The Cloud Operating Model includes standardizing, automating, and streamlining the processes for the creation, management, and delivery of cloud services. This includes the processes for requests, approvals, and provisioning, as well as those for decommissioning, security, and compliance. The focus of the Cloud Operating Model is to create a streamlined process to deliver high-quality, secure, and compliant cloud services, while reducing the time to market.

A key process in the Cloud Operating Model is the creation and management of a Cloud Service Catalog. The Cloud Service Catalog is a central repository of all the cloud services that are offered, either as standard services or as custom services. This catalog should be accessible to all stakeholders and should provide the necessary information to enable them to make informed decisions about which services to consume. The Cloud Service Catalog should include detailed information about the services, their costs, and any relevant security and compliance information.

Automation is a key component of the Cloud Operating Model, as it helps to reduce the time required to deliver and manage cloud services. Automation also helps to reduce the risk of human error, and to ensure consistent and repeatable results. Automation can be used to automate the process of request and approval, as well as the process of provisioning, deprovisioning, and decommissioning of services. The Cloud Operating Model should leverage the capabilities of the Cloud Platform to automate as much of the process as possible.

## Technology
The Cloud Operating Model leverages the technology capabilities of the Cloud Platform to deliver the services and to manage the infrastructure that supports the services. The Cloud Platform is the foundation of the Cloud Operating Model and provides the technical capabilities required to support the following types of cloud services: Private Cloud Services, Brokered Services, and Public Cloud Services.
The technology components of the Cloud Operating Model should include the tools required to manage the Cloud Service Catalog, the tools required to automate the processes, and the tools required to manage the infrastructure. This may include tools for configuration management, infrastructure as code, continuous integration and continuous delivery, and automated testing.

The Cloud Platform should also provide the necessary security and compliance capabilities to meet the requirements of the organization. This may include security tools, such as firewalls, intrusion detection and prevention, and data encryption. The Cloud Operating Model should also leverage the platform’s security capabilities to ensure that all services are delivered in a secure and compliant manner.

## Final Thoughts
The Cloud Operating Model is a blueprint for delivering high-quality, secure, and compliant cloud services. It includes the people, processes, and technology required to create, manage, and deliver cloud services, leveraging the capabilities of the Cloud Platform. By creating a well-defined Cloud Operating Model, organizations can ensure that they are delivering the best possible cloud services to their customers and stakeholders.