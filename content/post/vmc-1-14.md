---
title: "VCP-VMC 1.14: Describe the purpose of using Kubernetes" # Title of the blog post.
date: 2023-03-02T12:10:20-05:00 # Date of post creation.
description: "Article description." # Description used for search engine.
featured: false # Sets if post is a featured post, making appear on the home page side bar.
draft: false # Sets whether to render this page. Draft of true will not be rendered.
toc: true # Controls if a table of contents should be generated for first-level links automatically.
# menu: main
usePageBundles: false # Set to true to group assets like images in the same folder as this post.
featureImage: "/images/vfrazier-cover.png" # Sets featured image on blog post.
featureImageAlt: 'Description of image' # Alternative text for featured image.
thumbnail: "/images/vfrazier-cover.png" # Sets thumbnail image appearing inside card on homepage.
shareImage: "/images/vfrazier-cover.png" # Designate a separate image for social media sharing.
codeMaxLines: 10 # Override global value for how many lines within a code block before auto-collapsing.
codeLineNumbers: false # Override global value for showing of line numbers within code block.
figurePositionShow: true # Override global value for showing the figure label.
categories:
  - VCP-VMC
tags:
  - VMware Cloud Professional
  - Kubernetes
  - Tanzu Kubernetes Grid
  - Container Orchestration
  - Cloud Native Computing Foundation
series:
  - VCP-VMC Prep
# comment: false # Disable comment if false.
---

As a candidate preparing for the VMware Cloud Professional: VMware Cloud exam, understanding the purpose of using Kubernetes in a VMware environment is essential. Kubernetes is an open-source container orchestration system that automates the deployment, scaling, and management of containerized applications. It allows you to run your applications on a cluster of servers that can span across different cloud providers or on-premises environments.

## Containers
One of the main benefits of Kubernetes is container orchestration savings. Kubernetes automates many of the DevOps processes that were previously handled manually, such as deploying, scaling, updating, and monitoring containers. This reduces the operational overhead and complexity of managing containerized applications. Kubernetes also facilitates the development and deployment of microservices, which are small, independent, and loosely coupled services that communicate with each other. Microservices enable faster delivery, easier testing, and greater scalability of applications.

Tanzu Kubernetes Grid (TKG) is a Kubernetes cluster provisioning engine that integrates with VMware infrastructure. TKG allows you to create and scale clusters according to your needs using a command-line interface or a graphical user interface. TKG integrates with VMware Cloud Foundation Services, which provide consistent networking, storage, security, and management capabilities across different cloud environments. TKG supports deploying both management clusters and workload clusters. A management cluster is a dedicated cluster that hosts the Cluster API components and other tools for creating and managing workload clusters. A workload cluster is a user-facing cluster that runs your applications and services. TKG provides an enterprise-ready Kubernetes runtime that is compatible with upstream Kubernetes releases and certified by the Cloud Native Computing Foundation (CNCF).

## Power of Tanzu
By using Tanzu Kubernetes Grid, you can give developers secure, self-service access to fully compliant and conformant Kubernetes clusters on premises and in public clouds. You can also leverage existing tools and workflows to build, test, and deploy applications faster. Tanzu also simplifies container management with vSphere Pods, which run containers directly on the hypervisor without VMs. Additionally, Tanzu delivers high performance and resource efficiency for Kubernetes workloads by following best practices and leveraging VMware Cloud Foundation Services.

When comparing Tanzu to other solutions, it's essential to consider factors such as cloud and platform compatibility, feature set and capabilities, ease of use and automation, performance and scalability, and cost and licensing. Tanzu is one of the leading container orchestration solutions that competes with other products such as Red Hat OpenShift, Amazon ECS, and HPE Ezmeral.

## Use Cases
There are several use cases for Kubernetes in VMware, including provisioning Kubernetes clusters, deploying container-based applications, integrating Kubernetes with cloud services, monitoring Kubernetes stacks, and securing Kubernetes environments. By using Kubernetes in VMware, you can provision and manage standard Kubernetes clusters, package and deploy applications easily using Helm charts, integrate your Kubernetes clusters with various cloud services and APIs, monitor the performance and health of your Kubernetes clusters and workloads, and secure your applications and data using features such as encryption, network isolation, RBAC, pod security policies, secrets management, and more.

Understanding the purpose of using Kubernetes in a VMware environment is crucial for preparing for the VMware Cloud Professional: VMware Cloud exam. Tanzu Kubernetes Grid is an enterprise-ready Kubernetes runtime that integrates seamlessly with VMware infrastructure and provides several benefits, including streamlined development, accelerated innovation, agile operations, optimized performance, enhanced security, and reduced costs. By leveraging Kubernetes in VMware, you can provision and manage Kubernetes clusters, deploy container-based applications, integrate with cloud services, monitor your stacks, and secure your environments effectively. With this knowledge, you'll be better prepared to take the VMware Cloud Professional: VMware Cloud exam and succeed in your career as a VMware professional.
