---
title: "Day 1: Here We Go!" # Title of the blog post.
date: 2023-01-27T14:21:01-05:00 # Date of post creation.
description: "Looking to optimize your homelab or get VMware certified? Look no further! Frazier, a technical curriculum and certification writer, shares valuable insights and tips on his blog. From optimizing your homelab to solving common problems, Frazier provides guided tutorials and solutions. For those interested in VMware certifications, he breaks down requirements and provides pointers on how to prepare and get certified. Frazier also explores unique solutions for work and play, sharing his insights on how to use new tools, hacks, and innovative applications to your advantage. Whether you're a beginner or an expert, join Frazier on this journey to explore homelabs and VMware certifications." # Description used for search engine.
featured: true # Sets if post is a featured post, making appear on the home page side bar.
draft: false # Sets whether to render this page. Draft of true will not be rendered.
toc: false # Controls if a table of contents should be generated for first-level links automatically.
# menu: main
usePageBundles: false # Set to true to group assets like images in the same folder as this post.
featureImage: "/images/featured/vmc/001.png" # Sets featured image on blog post.
featureImageAlt: 'Description of image' # Alternative text for featured image.
featureImageCap: 'This is the featured image.' # Caption (optional).
thumbnail: "/images/featured/vmc/001.png" # Sets thumbnail image appearing inside card on homepage.
shareImage: "/images/featured/vmc/001.png" # Designate a separate image for social media sharing.
codeMaxLines: 10 # Override global value for how many lines within a code block before auto-collapsing.
codeLineNumbers: false # Override global value for showing of line numbers within code block.
figurePositionShow: true # Override global value for showing the figure label.
categories:
  - vFrazier
tags:
  - About
# comment: false # Disable comment if false.
---

Welcome to my site! If you're looking to get the most out of your homelab or interested in VMware certifications, you've come to the right place. As someone who has written technical curriculum and certifications, I'm excited to share my insights and experiences with you.

Through my blog, I'll be sharing my knowledge and tips on how to optimize your homelab and solve common problems that may arise. I'll also provide guided tutorials on how to implement different solutions and configurations.

For those interested in VMware certifications, I'll break down the requirements of popular certifications and provide helpful pointers on how to prepare and get certified. With my experience writing certifications and technical curriculum, I understand what it takes to succeed in these programs.

In addition to homelab tips and VMware certification guidance, I'll be exploring unique solutions that you can use for both work and play. I'll share my insights on how to use new tools, cool hacks, and innovative applications to your advantage.

So join me on this journey, and let's explore the world of homelabs and VMware certifications together. Whether you're a beginner or an expert, there's something here for everyone. Let's get started!

![:inline](images/signature.png)
