---
title: "VCP-VMC 1.12: Explain scaling options in VMware Cloud environments" # Title of the blog post.
date: 2023-03-01T14:22:36-05:00 # Date of post creation.
description: "Article description." # Description used for search engine.
featured: false # Sets if post is a featured post, making appear on the home page side bar.
draft: false # Sets whether to render this page. Draft of true will not be rendered.
toc: true # Controls if a table of contents should be generated for first-level links automatically.
# menu: main
usePageBundles: false # Set to true to group assets like images in the same folder as this post.
featureImage: "/images/featured/vmc/010.png" # Sets featured image on blog post.
featureImageAlt: 'Description of image' # Alternative text for featured image.
featureImageCap: 'This is the featured image.' # Caption (optional).
thumbnail: "/images/featured/vmc/010.png" # Sets thumbnail image appearing inside card on homepage.
shareImage: "/images/featured/vmc/010.png" # Designate a separate image for social media sharing.
codeMaxLines: 10 # Override global value for how many lines within a code block before auto-collapsing.
codeLineNumbers: false # Override global value for showing of line numbers within code block.
figurePositionShow: true # Override global value for showing the figure label.
categories:
  - Exam Prep
tags:
  - VMware Cloud
  - VMware Cloud on AWS
  - Cloud Scalability
  - Elastic DRS
  - Virtualization
# comment: false # Disable comment if false.
---

## Introduction
As businesses of all sizes look to the cloud for solutions to their IT needs, cloud scalability is becoming increasingly important. Cloud scalability is the ability to increase or decrease IT resources as needed to meet changing demand. This is a major advantage of cloud computing, allowing businesses to quickly and easily scale their IT infrastructure with minimal disruption or downtime. In this blog post, we’ll discuss cloud scalability, the different types of scalability, the benefits of cloud scalability, when to use cloud scalability, and how to achieve cloud scalability.

## What is cloud scalability?
Cloud scalability in cloud computing refers to the ability to increase or decrease IT resources as needed to meet changing demand. It’s one of the major advantages of cloud computing, as businesses can quickly and easily scale their IT infrastructure with minimal disruption or downtime. Data storage capacity, processing power and networking can all be scaled using existing cloud computing infrastructure. It’s important to note the difference between cloud scalability and cloud elasticity. Cloud elasticity refers to a system’s ability to grow or shrink dynamically in response to changing workload demands. This is ideal for businesses with variable and unpredictable workloads. Cloud scalability, on the other hand, refers to its ability to increase workload with existing hardware resources. This is ideal for businesses with predictable workloads.

## Why is cloud scalable?
Cloud scalability is made possible through virtualization. Virtual machines (VMs) are highly flexible and can be easily scaled up or down. This is in contrast to physical machines whose resources and performance are relatively set. Furthermore, third-party cloud providers have all the vast hardware and software resources already in place to allow for rapid scaling that an individual business could not achieve cost-effectively on its own.

## Benefits of cloud scalability
The major benefits of cloud scalability are: convenience, flexibility and speed, cost savings, and disaster recovery. Cloud scalability allows IT to quickly respond to changing demands, while avoiding the upfront costs of purchasing expensive equipment. It also enables businesses to reduce disaster recovery costs by eliminating the need for building and maintaining secondary data centers. ## When to use cloud scalability Cloud scalability is ideal for businesses looking for the flexibility to quickly respond to changing demands. Successful businesses employ scalable business models that allow them to grow quickly and meet changing demands. Cloud scalability advantages help businesses stay nimble and competitive.

## How to achieve cloud scalability?
Businesses have many options for how to set up a customized, scalable cloud solution, such as public cloud, private cloud, or hybrid cloud. There are two basic types of scalability in cloud computing: vertical and horizontal scaling. With vertical scaling, you add or subtract power to an existing cloud server, while horizontal scaling (scaling in or out) involves adding more servers to spread out the workload. To determine a right-sized solution, ongoing performance testing is essential. IT administrators must continually measure factors such as response time, number of requests, CPU load and memory usage. Automation can also help optimize cloud scalability, with thresholds for usage that trigger automatic scaling.

VMware Cloud provides the scalability, automation, and performance testing needed to ensure that businesses maintain the performance and availability required for their applications. With its easy-to-use cloud scalability features, VMware Cloud makes it easy for businesses to quickly and cost-effectively scale their cloud infrastructure as demands change.

## How do you scale with VMware Cloud on AWS?
The VMware Cloud on AWS service is operated and managed by VMware, taking the infrastructure operations burden away from the customer. One such feature that further extends availability and resiliency of the SDDC cluster, which is exclusive to VMware Cloud on AWS, is Elastic DRS (eDRS).

Elastic DRS allows you to scale your cluster in response to demand, or lack of demand, by adding or removing hosts automatically based on specific policies that are configured. The eDRS algorithm runs every 5 minutes and looks at predefined resource thresholds for CPU, memory, and storage. If any of the resources consistently remain above the defined threshold, a scale-up recommendation alert is generated, and a host is added to the cluster. Conversely, a scale-down recommendation alert is only generated when all resources are consistently below the threshold, triggering the removal of a host.

By default, the Scale Up for Storage Only policy is now configured for every cluster deployed within your SDDC. The maximum usable capacity of your vSAN datastore is 75%; when you reach that threshold, eDRS will automatically start the process of adding a host to your cluster and expanding your vSAN datastore.

The other policies available include Optimize for Best Performance and Optimize for Lowest Cost. In these scenarios, the eDRS algorithm will look at the minimum and maximum hosts you’ve specified for your cluster size and take that into consideration with resource consumption. Optimizing for performance adds hosts quickly and removes them slowly to ensure the best possible performance; while optimizing for lowest cost removes hosts quickly and adds hosts slower to keep costs to a minimum.

The resource thresholds differ based on the policy you configure.

There is a safety check built-in for eDRS, so we aren’t continuously adding or removing hosts. There is a 30-minute delay between scale-up events, and a 3-hour delay to trigger a scale-down event after a scale-up event. When scaling recommendations are generated, the multi-channel notification service will send out automated notifications via email to organization members. In the end, you have the scalability and flexibility you expect from a Cloud service to maintain availability, capacity, and performance. With elastic DRS, VMware Cloud on AWS customers can rest assured that their clusters are always running optimally and efficiently.

## Final Thoughts

Cloud scalability is a major advantage of cloud computing, allowing businesses to quickly and easily scale their IT infrastructure with minimal disruption or downtime. It is made possible through virtualization and third-party cloud providers, and it offers businesses convenience, flexibility and speed, cost savings, and disaster recovery.

VMware Cloud on AWS provides the scalability and automation needed to ensure that businesses maintain the performance and availability required for their applications. Through its elastic DRS feature, VMware Cloud on AWS customers can take advantage of scalability and flexibility to maintain availability, capacity, and performance.

## Review Questions

1.  What is the difference between cloud scalability and cloud elasticity?
2.  Name three benefits of cloud scalability?
3.  What are the two basic types of scalability in cloud computing?
4.  What features does VMware Cloud on AWS provide to ensure businesses maintain the required performance and availability?
5.  How does Elastic DRS help optimize cloud scalability?
