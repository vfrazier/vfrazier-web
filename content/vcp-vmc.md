---
title: "VMware Certified Professional - VMware Cloud"
date: 2023-03-05T07:57:30-05:00
draft: false
---
Welcome to my VMware Cloud certification preparation series! In this series of posts, we will be reviewing each of the exam blueprint objectives to help learners prepare for the VMware Cloud certification.

The VMware Cloud certification is a highly sought-after credential in the field of cloud computing, and it demonstrates an individual's expertise in designing, building, and managing VMware Cloud environments. The certification exam is designed to test a candidate's knowledge and skills in various areas such as cloud infrastructure, security, and disaster recovery. Our goal is to provide learners with a comprehensive understanding of the exam objectives and help them feel confident and prepared for the certification exam.

So, whether you're new to VMware Cloud or have some experience, this series is for you. Let's get started!

{{% notice note "Content Updating Weekly" %}}
As we create new content weekly, this page will be updated with each of the outlined topics.
{{% /notice %}}

## Section 1 – Architecture and Technologies
- Explain the benefits of cloud computing
- Describe the functional components of a VMware Cloud solution
- Differentiate between VMware Cloud connectivity options
- Describe a cloud network architecture
- Describe networking in the software-defined data center (SDDC)
- Describe VMware SDDC components
- Explain Hybrid Linked Mode for the VMware SDDC
- Describe virtual machine components
- Describe VMware vSphere vMotion and vSphere Storage vMotion technology
- Explain a high availability and resilient infrastructure
- Describe the different backup and disaster recovery options for VMware Cloud
- Explain scaling options in VMware Cloud environments
- Identify authentication options for the VMware Cloud Services Portal
- Describe the purpose of using Kubernetes
- Describe use cases for VMware Cloud on Dell EMC and VMware Cloud on AWS Outposts

## Section 2 – VMware Products and Solutions
- Describe the VMware Cloud operating model
- Identify the role of other cloud services
- Explain the VMware multi-cloud vision
- Identify the appropriate backup or disaster recovery method for VMware Cloud given a scenario
- Describe how VMware and its hyperscaler partners address IT challenges
- Recognize VMware Cloud use cases
- Describe the function of VMware HCX
- Explain the NSX architecture in VMware Cloud
- Explain the functions of Kubernetes components
- Describe the functions of VMware Tanzu products in Kubernetes life cycle management.
- Explain Tanzu Kubernetes Grid concepts 

## Section 3 – Planning and Designing
- Understand configuration sizing requirements for a VMware Cloud SDDC
- Understand considerations for installing VMware Cloud on Dell EMC and VMware Cloud on AWS Outposts on-premises

## Section 4 – Installing, Configuring, and Setup
- Deploy and configure VMware HCX appliances
- Configure connectivity between clouds (VPN, AWS Direct Connect, VMware Managed Transit Gateway)
- Set up Hybrid Linked Mode using the VMware Cloud Gateway Appliance
- Deploy and configure cloud business continuity and disaster recovery (BC/DR) solutions
- Assess the requirements for cloud onboarding within a VMware single- or multi-cloud environment
- Assess the required account access and privileges for an SDDC deployment within a VMware single- or multi-cloud environment
- Understand the concept of different types of segments (compute and management)
- Understand hyperscaler networking considerations
- Understand the concept of dynamic SDDC scale-out
- Complete cluster operations

## Section 5 – Performance-tuning and Optimization
- Determine networking performance
- Determine storage performance
- Optimize the guest OS configuration

## Section 6 – Troubleshooting and Repairing
- Troubleshoot networking issues
- Troubleshoot internetworking
- Troubleshoot security
- Troubleshoot workloads
- Troubleshoot storage

## Section 7 – Administrative and Operational Tasks
- Create and manage user account and role permissions
- Create a content library
- Create and manage network segments
- Create and manage VM snapshots
- Monitor VMware NSX networking within VMware Cloud
- Determine the appropriate network connectivity option for connecting to and from VMware Cloud
- Recognize management and operational responsibilities in VMware Cloud on AWS
- Describe elements of the service management process
- Recognize update and upgrade responsibilities of various components for VMware Cloud on AWS

